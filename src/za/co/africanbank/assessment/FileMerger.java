package za.co.africanbank.assessment;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class FileMerger {
	
	private Set<String> ipAddressesSet;
	private Map<String, Set<Integer>> ipAndNumbersMap; 
	
	public FileMerger() {
		ipAddressesSet = new TreeSet<String>();
		ipAndNumbersMap = new HashMap<String, Set<Integer>>();
	}
	
	
	public void printMergedList(String file1, String file2) {
		readFile(file1);
		readFile(file2);
		printList();
	}
	
	private void printList() {
		for(String ipAddress : ipAddressesSet) {
			System.out.print(ipAddress + " : ");
			for(Integer number : ipAndNumbersMap.get(ipAddress)) {
				System.out.print(number + ",");
			}
			System.out.println();
		}
	}
	
	private void readFile(String fileName) {
		
		try {
			List<String> allLines = Files.readAllLines(Paths.get(fileName));
			for (String line : allLines) {
				
				String[] lineSplit = line.split(":");
							
				String[] numbersArray = lineSplit[1].split(",");
				
				String ipAddress = lineSplit[0];
				
				ipAddressesSet.add(ipAddress);
				
				mapNumbersToIpaddress(ipAddress, Arrays.asList(numbersArray));
			}
			
		} catch (IOException e) {
			System.out.println("Error : "+ e.toString());
			//e.printStackTrace(System.out);
		}
	}
	
	private void mapNumbersToIpaddress(String ipAddress, List<String> numbersList) {
		
		if(!ipAndNumbersMap.containsKey(ipAddress)) {
			ipAndNumbersMap.put(ipAddress, new TreeSet<Integer>() );
		}
		
		Integer numberValue = null;
		for(String number : numbersList) {
			try {
				numberValue = Integer.valueOf(number.trim());
				ipAndNumbersMap.get(ipAddress).add(numberValue);
			}catch(NumberFormatException nfe) {
				System.out.println("Value of : \""+number+"\" is not a number");
			}
			
		}
	}

}
