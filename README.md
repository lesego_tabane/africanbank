Utility application to merge 2 files on IP Addresses and print the results

To run the application:

run mvn clean install

then go to the target folder and locate the Filemanagerutility jar file

to execute this, you have to run: java -jar FileMergeUtility-x.x.x.jar path/to/file1 path/to/file2